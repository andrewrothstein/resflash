#!/bin/sh

# Save SSH, IKE, and SOII keys to /cfg
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

echo 'Saving SSH, IKE, and SOII keys'

mount -s /cfg
trap 'sync; umount /cfg; exit 1' ERR INT

mkdir -p /cfg/{etc,var,tmp} /cfg/upgrade_overlay/{etc,home,root,var}
chmod 700 /cfg/upgrade_overlay/root

# tar -C doesn't play nicely with glob(3)
(
  cd /etc
  tar cf - ssh/ssh_host_*key*|tar xvpf - -C /cfg/etc
  tar cf - {isakmpd,iked}/local.pub|tar xvpf - -C /cfg/etc
  tar cf - {isakmpd,iked}/private/local.key|tar xvpf - -C /cfg/etc
  tar cf - soii.key|tar xvpf - -C /cfg/etc
)

sync
umount /cfg
