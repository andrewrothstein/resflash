#!/bin/sh

# Build and configure primary filesystem
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

FS=resflash-${MACHINE}-${imgsizemb}MB-${COM0}${DATE}.fs
echo "Creating filesystem: ${FS}"

# Build the filesystem

dd if=/dev/zero of=${FS} bs=1k count=0 seek=${fssizekb} status=none
get_next_vnd
fsvnd=${NEXTVND}
vnconfig ${fsvnd} ${FS} > /dev/null

# Newfs the filesystem

newfs -O 2 ${fsvnd}c >> ${BUILDPATH}/01.mkfs.00.newfs.fs 2>&1

# Mount and populate filesystem

echo 'Populating filesystem and configuring fstab'
fst=$(date +%s)
mkdir -p ${BUILDPATH}/fs
mount -o noatime,softdep /dev/${fsvnd}c ${BUILDPATH}/fs
tar cf - -C ${basedir} .|tar xpf - -C ${BUILDPATH}/fs
mkdir -p ${BUILDPATH}/fs/cfg ${BUILDPATH}/fs/mbr ${BUILDPATH}/fs/${DOSMNT}

# Add resflash hooks

mkdir -p ${BUILDPATH}/fs/resflash
cp -p ${BINDIR}/host/* ${BUILDPATH}/fs/resflash/
cp -p ${BINDIR}/resflash.sub ${BUILDPATH}/fs/resflash/
cp -p ${BINDIR}/LICENSE ${BUILDPATH}/fs/resflash/
touch ${BUILDPATH}/fs/resflash/.firsttime
chown -R root:wheel ${BUILDPATH}/fs/resflash

echo "version: ${VERSION}" >> ${BUILDPATH}/fs/resflash/BUILD
echo "build command: ${0} ${BUILDARGS}" >> ${BUILDPATH}/fs/resflash/BUILD
echo "build date: $(date)" >> ${BUILDPATH}/fs/resflash/BUILD

cp ${BINDIR}/etc/resflash.conf ${BUILDPATH}/fs/etc/
chown root:wheel ${BUILDPATH}/fs/etc/resflash.conf

cp -p ${BUILDPATH}/fs/etc/examples/rc.shutdown ${BUILDPATH}/fs/etc/
echo '/resflash/resflash.save' >> ${BUILDPATH}/fs/etc/rc.shutdown
echo '/resflash/resflash.relink' >> ${BUILDPATH}/fs/etc/rc.shutdown

sed -i -e '/.*_mp=$(mount -t ffs/s/ffs/ffs,mfs/' \
-e '/^rm.*fastboot$/a\
/resflash/rc.resflash\
# Re-read rc.conf and rc.conf.local from the new /etc\
_rc_parse_conf\
' -e '/^wait_reorder_libs$/a\
if mount -t mfs|grep -q /usr/share/relink; then umount /usr/share/relink; fi\
' ${BUILDPATH}/fs/etc/rc

chown root:wheel ${BUILDPATH}/fs/bsd*
chmod 600 ${BUILDPATH}/fs/bsd*

# Populate /dev

(cd ${BUILDPATH}/fs/dev && ./MAKEDEV all)

# Configure fstab

duid=$(disklabel ${imgvnd}|grep duid|cut -d ' ' -f 2)
echo "${duid}.a /mbr ffs rw,noatime,nodev,noexec,noauto 0 2" >> \
${BUILDPATH}/fs/etc/fstab
if [ ${swapsizemb} -gt 0 ]; then
  echo "${duid}.b none swap sw" >> ${BUILDPATH}/fs/etc/fstab
fi
echo "${duid}.d / ffs ro,noatime 0 1" >> ${BUILDPATH}/fs/etc/fstab
echo "${duid}.f /cfg ffs rw,noatime,nodev,noexec,noauto 0 2" >> \
${BUILDPATH}/fs/etc/fstab
echo "${duid}.i /${DOSMNT} msdos rw,noatime,nodev,noexec,noauto 0 2" >> \
${BUILDPATH}/fs/etc/fstab
echo 'swap /tmp mfs rw,noatime,nodev,nosuid,-s32M 0 0' >> \
${BUILDPATH}/fs/etc/fstab

# Install random.seed and host.random

dd if=/dev/random of=${BUILDPATH}/fs/etc/random.seed bs=512 count=1 status=none
chmod 600 ${BUILDPATH}/fs/etc/random.seed
dd if=/dev/random of=${BUILDPATH}/fs/var/db/host.random bs=65536 count=1 \
status=none
chmod 600 ${BUILDPATH}/fs/var/db/host.random

# Set com0 ttys on arches that default to ttyC*, if directed

if [ -n "${com0sp+1}" ] && [ ${MACHINE} != 'arm64' ]; then
  sed -i -e "/^tty00/s/std\.9600/std\.${com0sp}/" \
      -e '/^tty00/s/unknown off/vt220 on secure/' \
      ${BUILDPATH}/fs/etc/ttys
fi

# Populate arm64 boot scaffold, if applicable

if [ ${MACHINE} == 'arm64' ]; then
  mkdir -p ${BUILDPATH}/fs/usr/mdec/rpi/overlays ${BUILDPATH}/fs/usr/mdec/pine64

  cp /usr/local/share/raspberrypi-firmware/boot/bcm2710-rpi-2-b.dtb \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/bcm2710-rpi-3-b.dtb \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/bcm2710-rpi-3-b-plus.dtb \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/bcm2710-rpi-cm3.dtb \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/bcm2711-rpi-4-b.dtb \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/bcm2711-rpi-400.dtb \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/bcm2711-rpi-cm4.dtb \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/bootcode.bin \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/start.elf \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/start4.elf \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/fixup.dat \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/fixup4.dat \
  ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/raspberrypi-firmware/boot/overlays/disable-bt.dtbo \
  ${BUILDPATH}/fs/usr/mdec/rpi/overlays/
  cp /usr/local/share/u-boot/rpi_arm64/u-boot.bin ${BUILDPATH}/fs/usr/mdec/rpi/
  cp /usr/local/share/u-boot/pine64_plus/u-boot-sunxi-with-spl.bin \
  ${BUILDPATH}/fs/usr/mdec/pine64/
fi

# Set root password in the form 'resflashYYYYMMDD'

sed -i "/^root/s|root::|root:$(echo resflash$(echo ${DATE}|cut -d _ -f 1)|\
encrypt -b 10):|" ${BUILDPATH}/fs/etc/master.passwd
pwd_mkdb -p -d ${BUILDPATH}/fs/etc master.passwd

# Work around clang hard links crossing mfs mounts

rm ${BUILDPATH}/fs/usr/libexec/cpp
ln -s /usr/bin/clang ${BUILDPATH}/fs/usr/libexec/cpp

# Relocate libLLVM.so to save space in /usr/lib

mkdir -p ${BUILDPATH}/fs/usr/libllvm
mv ${BUILDPATH}/fs/usr/lib/libLLVM.so.*.? ${BUILDPATH}/fs/usr/libllvm/
ln -s /usr/libllvm/$(ls -r ${BUILDPATH}/fs/usr/libllvm/|head -n 1) \
${BUILDPATH}/fs/usr/lib

# Run fw_update in requested mode: all, ucode, or none

cp -p /etc/resolv.conf ${BUILDPATH}/fs/etc/

if [ ${CROSSARCH} -eq 0 ]; then
  st=$(date +%s)

  cp -p /var/run/dmesg.boot ${BUILDPATH}/fs/var/run/
  case ${fwupdate} in
    ALL) echo -n '  Running fw_update'
         DESTDIR=${BUILDPATH}/fs fw_update -avv >> \
         ${BUILDPATH}/01.mkfs.01.fw_update 2>&1;;
    UCODE) echo -n '  Running fw_update: amd intel'
           DESTDIR=${BUILDPATH}/fs fw_update -vv amd intel >> \
           ${BUILDPATH}/01.mkfs.01.fw_update 2>&1;;
  esac

  rm ${BUILDPATH}/fs/var/run/dmesg.boot
  if [ ${fwupdate} != 'NONE' ]; then
    echo " ($(($(date +%s) - st))s)"
  fi
else
  echo '*** WARNING: fw_update not supported in cross-arch builds, skipping.' \
  '***'
fi

# Perform chroot activities (syspatch, packages)

. ${BINDIR}/mkchroot.sh
rm ${BUILDPATH}/fs/etc/resolv.conf

# Unmount and free filesystem for copy

df -h ${BUILDPATH}/fs >> ${BUILDPATH}/01.mkfs.02.df
sync
umount ${BUILDPATH}/fs
vnconfig -u ${fsvnd}

echo "Populating filesystem complete ($(($(date +%s) - fst))s)"

# Calculate checksum

echo -n 'Calculating filesystem checksum'
st=$(date +%s)
cksum -a ${ALG} -h ${FS}.cksum ${FS}

if [ -n "${XXHALG+1}" ]; then
  # Pass -q to xxhsum to prevent it from clearing the line
  xxhsum -H${XXHALG} -q ${FS} > ${FS}.xxhsum
fi
echo " ($(($(date +%s) - st))s)"

# Write filesystem to image's d partition

echo -n 'Writing filesystem to disk image'
st=$(date +%s)
dd if=${FS} of=/dev/r${imgvnd}d bs=1m >> ${BUILDPATH}/01.mkfs.03.dd.fstoimg 2>&1
echo " ($(($(date +%s) - st))s)"
